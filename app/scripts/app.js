angular.module('myApp', [
		'ngResource',
		'ngRoute'
	])
	.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
		$routeProvider
			.when('/', {
				templateUrl: '/views/main.html',
				controller: 'MainCtrl'
//				css: ['styles/style.css']
			})
			.otherwise({
				redirectTo: 'views/main.html'
			});

		$locationProvider.html5Mode(false);
	}]);