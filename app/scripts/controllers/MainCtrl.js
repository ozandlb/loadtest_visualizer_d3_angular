angular.module('myApp')

	.controller('MainCtrl', ['$scope', '$parse', 'LoadTestManager', function($scope, $parse, LoadTestManager)  {

		$scope.loadTests = [];

		$scope.loadFileContent = function($fileContent){

			LoadTestManager.createLoadTest($fileContent).then(function(res) {
				$scope.loadTests[$scope.loadTests.length] = res;
				console.log($scope.loadTests);
			});
		};


	}]);