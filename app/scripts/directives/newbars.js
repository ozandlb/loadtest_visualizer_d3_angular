angular.module('myApp')

	.directive('newbars', ['$parse', function($parse) {
		return {
			restrict: 'E',
			replace: true,
			template: '<div id="awesomebar"></div>',
			link: function (scope, element, attrs) {
				var data = attrs.data.split(','),
					chart = d3.select("#awesomebar")
						.append("div").attr("class", "awesomebar")
						.selectAll("div")
						.data(data).enter()
						.append("div")
						.transition().ease("elastic")
						.attr("class", "bars")
						.style("height", function(d) {
							var barheight = d;
							return barheight + "px";
						});
			}
		}

	}]);