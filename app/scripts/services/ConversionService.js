angular.module('myApp')

	.factory('ConversionService', ['$q', function($q) {

		var jtlHeaders = [
			'Start Time',
			'Sample Time (ms)',
			'Label',
			'Response Code',
			'Response Message',
			'Thread Name',
			'Text?',
			'True?',
			'Bytes',
			'Latency'];


		var papaConfig = {
			delimiter: '',	// auto-detect
			newline: '',	// auto-detect
			header: true,
			dynamicTyping: true,
			preview: 0,
			encoding: "",
			worker: false,
			comments: false,
			step: undefined,
			complete: undefined,
			error: undefined,
			download: false,
			skipEmptyLines: false,
			chunk: undefined,
			fastMode: false
		};


		var CSVToJSON = function(csv) {

			var deferred = $q.defer();
			deferred.resolve(Papa.parse(csv, papaConfig).data);
			return deferred.promise;

		};


		return {
			CSVToJSON : CSVToJSON
		};


	}]);