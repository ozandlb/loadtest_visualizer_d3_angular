angular.module('myApp')

	.factory('LoadTest', ['ConversionService', '$q', function(ConversionService, $q) {

		var getUniqueVals = function(mlist) {
			var listToReturn = [];
			for (var i=0; i<mlist.length; i++) {
				if(listToReturn.indexOf(mlist[i]) === -1) {
					listToReturn.push(mlist[i]);
				}
			}
			return listToReturn;
		};


		var getAllLabels = function(data) {
			var deferred = $q.defer();
			var allLabels = _.pluck(data, 'Label');
			deferred.resolve(getUniqueVals(allLabels));
			return deferred.promise;
		};


		// constructor
		var LoadTest = function() {
			this.results = [];
			this.labels = [];
		};



		LoadTest.prototype.sortDataByLabel = function() {
			var self = this;
			var deferred = $q.defer();
			var indexedDataArray = _.groupBy(self.results, function(singleCall) {
				return singleCall.Label;
			});
			deferred.resolve(indexedDataArray);
			return deferred.promise;
		};


		LoadTest.prototype.parseData = function(fileContent) {
			var self = this;  // keep reference to this before changed by callbacks
			var deferred = $q.defer();
			ConversionService.CSVToJSON(fileContent).then(function(res) {
				self.results = res;
				getAllLabels(res).then(function(res2) {
					self.labels = res2;
//					console.log(self.labels);
					self.sortDataByLabel().then(function(res3) {
						self.indexedData = res3;
						console.log(res3);
						deferred.resolve(self);
					});
				});
			});
			return deferred.promise;
		};



		LoadTest.prototype.getAllLabels = function() {
			var self = this;
			return self.labels;
		};



		return LoadTest;


	}]);