angular.module('myApp')

	.factory('LoadTestManager', ['LoadTest', '$q', function(LoadTest, $q) {

		var LoadTests = [];

		var createLoadTest = function(fileContent) {
			var deferred = $q.defer();
			var thisTest = new LoadTest;
			thisTest.parseData(fileContent).then(function(res) {
				deferred.resolve(res);
			});
			LoadTests.push(thisTest);
			return deferred.promise;
		};

		var getLoadTests = function() {
			return LoadTests;
		};


		return {
			createLoadTest : createLoadTest,
			getLoadTests : getLoadTests
		}


	}]);